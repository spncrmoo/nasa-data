import { shallowMount } from "@vue/test-utils";
import SearchFilter from "@/components/SearchFilter.vue";

describe("SearchFilter", () => {
  it("changes an item's selection when it is clicked", () => {
    const wrapper = shallowMount(SearchFilter, {
      propsData: {
        options: [
          {
            label: "Images",
            value: "image",
            selected: true
          }
        ]
      }
    });
    wrapper.find(".checkbox-option").trigger("click");
    expect(wrapper.props().options[0].selected).toBe(false);
  });
});
