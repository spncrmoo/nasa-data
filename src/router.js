import Vue from "vue";
import Router from "vue-router";
import Search from "./views/Search.vue";
import Asset from "./views/Asset.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "*",
      redirect: "/search"
    },
    {
      path: "/search",
      name: "search",
      component: Search
    },
    {
      path: "/asset/:id",
      name: "asset",
      component: Asset
    }
  ]
});
