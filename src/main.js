import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

// Import axios for API consumption
import axios from "axios";
import VueAxios from "vue-axios";
Vue.use(VueAxios, axios);

// Add FontAwesome and only import necessary icons
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faSearch,
  faHeadphones,
  faPlay,
  faImage,
  faSync,
  faArrowLeft
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
library.add(faSearch, faHeadphones, faPlay, faImage, faSync, faArrowLeft);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
