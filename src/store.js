import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    options: [
      {
        label: "Images",
        value: "image",
        selected: true
      },
      {
        label: "Audio",
        value: "audio",
        selected: false
      }
    ],
    query: "",
    results: null,
    count: null,
    loading: false,
    currentPage: 1
  },
  mutations: {
    options(state, payload) {
      state.options = payload;
    },
    query(state, payload) {
      state.query = payload;
    },
    results(state, payload) {
      state.results = payload;
    },
    count(state, payload) {
      state.count = payload;
    },
    loading(state, payload) {
      state.loading = payload;
    },
    currentPage(state, payload) {
      state.currentPage = payload;
    }
  },
  actions: {
    // Dispatched in Search.vue
    getResults(context, payload) {
      Vue.axios
        .get(process.env.VUE_APP_API_URL + "/search", {
          params: {
            q: payload.query,
            media_type: payload.mediaTypes,
            page: payload.page
          }
        })
        .then(response => {
          context.commit("loading", false);
          context.commit("results", response.data.collection.items);
          context.commit("count", response.data.collection.metadata.total_hits);
        })
        .catch(e => {
          alert(e);
        });
    }
  }
});
